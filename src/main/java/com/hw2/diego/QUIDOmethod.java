package com.hw2.diego;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

public class QUIDOmethod {
	String sqlcommamd;
//==============================================================================
	Timestamp updateTime = new Timestamp(System.currentTimeMillis());
	String whereSql = " WHERE ID = 1";
	String updateSql = "UPDATE hw2 " + "SET Height = 183" + whereSql + updateTime + "' " + whereSql;

	public void orderData(Statement stmt) {
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sqlcommamd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {

			while (rs.next()) {
				char[] r = rs.getString(6).toCharArray();
				String hidename = null;
				if (r.length == 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1), "*");
				} else if (r.length >= 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1, r.length - 1), "*");
				}
				Employee emp = new Employee();
				emp.setId(rs.getInt("ID"));
				emp.setHeight(rs.getString("Height"));
				emp.setWeight(rs.getString("Weight"));
				emp.setEName(rs.getString("ENGname"));
				emp.setCName(hidename);
				emp.setExt(rs.getString("Ext"));
				emp.setEmail(rs.getString("Email"));
				emp.setBMI(rs.getFloat("BMI"));
				emp.setCreatetime(rs.getDate("CreateTime"));
				emp.setUpdatetime(rs.getDate("UpdateTime"));
				System.out.println(emp);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void updateData(Statement stmt) {
		try {
			stmt.executeUpdate(updateSql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteData(Statement stmt) throws SQLException {
		try {
			stmt.executeUpdate(sqlcommamd);
			System.out.println("data had deleted!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(stmt.executeUpdate(sqlcommamd));
	}

	public void queryData(Statement stmt) {

		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sqlcommamd);
			while (rs.next()) {
				char[] r = rs.getString(6).toCharArray();
				String hidename = null;
				if (r.length == 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1), "*");
				} else if (r.length >= 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1, r.length - 1), "*");
				}
				Employee emp = new Employee();
				emp.setId(rs.getInt("ID"));
				emp.setHeight(rs.getString("Height"));
				emp.setWeight(rs.getString("Weight"));
				emp.setEName(rs.getString("ENGname"));
				emp.setCName(hidename);
				emp.setExt(rs.getString("Ext"));
				emp.setEmail(rs.getString("Email"));
				emp.setBMI(rs.getFloat("BMI"));
				emp.setCreatetime(rs.getDate("CreateTime"));
				emp.setUpdatetime(rs.getDate("UpdateTime"));
				System.out.println(emp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void insertData(Connection conn) {
		String insertSQl = "INSERT INTO hw2 (ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI,CreateTime,UpdateTime) VALUES (?,?,?,?,?,?,?,?,?,?)";
		Timestamp updateTime = new Timestamp(System.currentTimeMillis());
		Timestamp createTime = new Timestamp(System.currentTimeMillis());
		try {
			PreparedStatement preparedStatement = conn.prepareStatement(insertSQl);
			preparedStatement.setInt(1, 57);
			preparedStatement.setString(2, "185");
			preparedStatement.setString(3, "80");
			preparedStatement.setString(4, "Diego-Chen");
			preparedStatement.setString(5, "陳文源");
			preparedStatement.setString(6, "1879");
			preparedStatement.setString(7, "putai111095@gmail.com");
			preparedStatement.setFloat(8, 23.0f);
			preparedStatement.setTimestamp(9, createTime);
			preparedStatement.setTimestamp(10, updateTime);
			preparedStatement.executeUpdate();
			System.out.println("success!!!");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
