package com.hw2.diego;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SetEmployeeData {
	private static List<Employee> EmployeeData = new ArrayList<Employee>();
	
	public void setDataIntoMySql(Connection conn) {
		String insertSQl = "INSERT INTO hw2 (ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI,CreateTime,UpdateTime) VALUES (?,?,?,?,?,?,?,?,?,?)";
		Timestamp updateTime = new Timestamp(System.currentTimeMillis());
		System.out.println(updateTime);
		Timestamp createTime = new Timestamp(System.currentTimeMillis());
		System.out.println(createTime);
		try {
			for (int i = 0; i < EmployeeData.size(); i++) {
				PreparedStatement preparedStatement = conn.prepareStatement(insertSQl);
				preparedStatement.setInt(1, EmployeeData.get(i).getId());
				preparedStatement.setString(2, EmployeeData.get(i).getHeight());
				preparedStatement.setString(3, EmployeeData.get(i).getWeight());
				preparedStatement.setString(4, EmployeeData.get(i).getEName());
				preparedStatement.setString(5, EmployeeData.get(i).getCName());
				preparedStatement.setString(6, EmployeeData.get(i).getExt());
				preparedStatement.setString(7, EmployeeData.get(i).getEmail());
				preparedStatement.setFloat(8, EmployeeData.get(i).getBMI());
				preparedStatement.setTimestamp(9, createTime);
				preparedStatement.setTimestamp(10, updateTime);
				preparedStatement.addBatch();
//				preparedStatement.executeUpdate();
				if (i % 10 == 9 || (i == EmployeeData.size() - 1)) {
					preparedStatement.executeBatch();
				}
//				System.out.println(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public void createEmployeeData() throws IOException {
		// create Employee arrayList
		try {
			String i;
			int ID = 1;
			// read data and split and set Data in list

			BufferedReader br = new BufferedReader(new FileReader("D:\\list.txt"));
			while ((i = br.readLine()) != null) {
				String[] tokens = i.split(" ");
				if (Integer.parseInt(tokens[0]) > 250 && Integer.parseInt(tokens[0]) < 100) {
					System.out.println(String.format("data '%d' height is wrong !!", ID));
				} 
				if (Integer.parseInt(tokens[1]) > 200 && Integer.parseInt(tokens[1]) < 30) {
					System.out.println(String.format("data '%d' weight is wrong !!", ID));
				}
				if (tokens[2].matches("[a-zA-Z]{1,30}-[a-zA-Z]{1,30}")) {

				} else {
					System.out.println(String.format("data '%d' English Name is wrong !!", ID));
				}
				if (tokens[3].matches("[\\u4E00-\\u9FA5]+")) {

				} else {
					System.out.println(String.format("data '%d' Chinenese Name is wrong !!", ID));
				}

				Employee temp = new Employee(ID, tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5],
						computBMI(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1])));
				EmployeeData.add(temp);
				ID++;
			}
			// System.out.println(list1);
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static float computBMI(float height, float weight) {
		float BMI;
		BMI = weight / ((height / 100) * (height / 100));
		return BMI;
	}
}
