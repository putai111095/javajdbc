package com.hw2.diego;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;

public class hw2 {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	//static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/emp_diego?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static final String DB_URL = "jdbc:mysql://10.67.67.186:3306/emp_diego?characterEncoding=utf8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2b8";
	static final String USER = "root";
	static final String PASS = "123456";
	public static void main(String[] args) throws IOException, ParseException, SQLException {
		QUIDOmethod quido = new QUIDOmethod();

//==Connection and Create Statement===================
		Connection conn = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

		} catch (ClassNotFoundException e) {
			System.out.println("error");
		}
		Statement stmt = conn.createStatement();
//==Set Data==========================================
		SetEmployeeData sed = new SetEmployeeData();
		sed.createEmployeeData();
		sed.setDataIntoMySql(conn);
//==Do Method============================================ 
		quido.insertData(conn);
//		quido.sqlcommamd="delete from hw2 where ID = 1";
//		quido.deleteData(stmt);
//		quido.sqlcommamd="SELECT * FROM hw2" + " ORDER BY Height ";
//		quido.orderData(stmt);
//		quido.sqlcommamd="select * from hw2 where ID = 3";
//		quido.queryData(stmt);
//		quido.updateData(stmt);
		//============================================//
		//orderData(orderSql,stmt);
		//updateData(stmt);
		//deleteData(deleteSql,stmt);
		//queryData(querySql,stmt);
		//setEmployeeData();
		//setDataintoMySql();
		//insertData(conn);
		stmt.close();
		conn.close();
	}
	public static void orderData(String orderSql, Statement stmt) {
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(orderSql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			
			while (rs.next()) {
				char[] r = rs.getString(6).toCharArray();
				String hidename = null;
				if (r.length == 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1), "*");
				} else if (r.length >= 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1, r.length - 1), "*");
				}
				Employee emp = new Employee();
				emp.setId(rs.getInt("ID"));
				emp.setHeight(rs.getString("Height"));
				emp.setWeight(rs.getString("Weight"));
				emp.setEName(rs.getString("ENGname"));
				emp.setCName(hidename);
				emp.setExt(rs.getString("Ext"));
				emp.setEmail(rs.getString("Email"));
				emp.setBMI(rs.getFloat("BMI"));
				emp.setCreatetime(rs.getDate("CreateTime"));
				emp.setUpdatetime(rs.getDate("UpdateTime"));
				System.out.println(emp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void updateData(Statement stmt) {
		Timestamp updateTime =new Timestamp(System.currentTimeMillis());
		String whereSql = " WHERE ID = 1";
		String updateSql = "UPDATE hw2 " + "SET Height = 183"+whereSql;
		String updateTimeSql =  "UPDATE hw2 "+"SET UpdateTime ='"+ updateTime+"' "+ whereSql;
		try {
			stmt.executeUpdate(updateSql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			stmt.executeUpdate(updateTimeSql);
			System.out.println("data had updated!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void deleteData(String sql, Statement stmt) throws SQLException {
		try {
			stmt.executeUpdate(sql);
			System.out.println("data had deleted!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String allData = "select * from hw2";
		ResultSet rs = stmt.executeQuery(allData);
		rs.last();
		int rowCount = rs.getRow();
		System.out.println(rowCount);
		rs.close();
	}

	public static void queryData(String sql,  Statement stmt) {
		try {
			// Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			// Statement stmt = conn.createStatement();
			// String sql = "select * from hw2 where ID = 1";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				char[] r = rs.getString(6).toCharArray();
				String hidename = null;
				if (r.length == 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1), "*");
				} else if (r.length >= 2) {
					hidename = rs.getString(6).replaceFirst(rs.getString(6).substring(1, r.length - 1), "*");
				}
				Employee emp = new Employee();
				emp.setId(rs.getInt(2));
				emp.setHeight(rs.getString(3));
				emp.setWeight(rs.getString(4));
				emp.setEName(rs.getString(5));
				emp.setCName(hidename);
				emp.setExt(rs.getString(7));
				emp.setEmail(rs.getString(8));
				emp.setBMI(rs.getFloat(9));
				emp.setCreatetime(rs.getDate(10));
				emp.setUpdatetime(rs.getDate(11));
				System.out.println(emp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public static void insertData(Connection conn) {
		String insertSQl = "INSERT INTO hw2 (ID,Height,Weight,ENGname,CHNname,Ext,Email,BMI,CreateTime,UpdateTime) VALUES (?,?,?,?,?,?,?,?,?,?)";
		Timestamp updateTime =new Timestamp(System.currentTimeMillis());
		System.out.println(updateTime);
		Timestamp createTime =new Timestamp(System.currentTimeMillis());
		System.out.println(createTime);
		try {			
			PreparedStatement preparedStatement = conn.prepareStatement(insertSQl);
			preparedStatement.setInt(1, 57);
			preparedStatement.setString(2, "185");
			preparedStatement.setString(3, "80");
			preparedStatement.setString(4, "Diego-Chen");
			preparedStatement.setString(5, "陳文源");
			preparedStatement.setString(6, "1879");
			preparedStatement.setString(7, "putai111095@gmail.com");
			preparedStatement.setFloat(8, 23.0f);
			preparedStatement.setTimestamp(9, createTime);
			preparedStatement.setTimestamp(10, updateTime);
			preparedStatement.executeUpdate();
//			System.out.println(row);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
