package com.hw2.diego;

import java.sql.Date;

public class Employee {
	private int id;
	private String height;
	private String weight;
	private String eName;
	private String cName;
	private String ext ;
	private String email;
	private float bmi;
	private Date createtime;
	private Date updatetime;
	public Employee() {
		
	}
	
	public Employee(int id, String height, String weight, String eName, String cName, String ext, String email,
			float bmi, Date createtime, Date updatetime) {
		super();
		this.id = id;
		this.height = height;
		this.weight = weight;
		this.eName = eName;
		this.cName = cName;
		this.ext = ext;
		this.email = email;
		this.bmi = bmi;
		this.setCreatetime(createtime);
		this.setUpdatetime(updatetime);
	}
	public Employee(int Id,String Height,String Weight,String EName,String CName,String Ext,String Email,float BMI) {
		this.id=Id;
		this.height=Height;
		this.weight=Weight;
		this.eName=EName;
		this.cName=CName;
		this.ext=Ext;
		this.email=Email;
		this.bmi=BMI;
	}

	public float getBMI() {
		return bmi;
	}
	public void setBMI(float bMI) {
		bmi = bMI;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getEName() {
		return eName;
	}
	public void setEName(String eName) {
		this.eName = eName;
	}
	public String getCName() {
		return cName;
	}
	public void setCName(String cName) {
		this.cName = cName;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String toString() {
		return "Employee[ID :" +  id+ ", Height:" +height  + ", Weight:" + weight + ", EName:" + eName +", CName:" + cName +", Ext:" + ext +", Email:" + email +", BMI:" + bmi + ",Createtime:"+createtime+",Updatetime:"+updatetime+"]"+"\n";
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	
}